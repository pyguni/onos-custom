// js for sample app view
(function () {
    'use strict';

    angular.module('ovFwdmon', [])
        .controller('OvFwdmonCtrl',
        ['$log', '$scope', 'TableBuilderService',

            function ($log, $scope, tbs) {
            	tbs.buildTable({
            		scope:$scope,
            		tag:'fwdmon'
            	});

                $log.log('OvFwdmonCtrl has been created');
            }]);
}());
