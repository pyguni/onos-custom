// js for sample app view
(function () {
    'use strict';

    angular.module('ovDistfwdmon', [])
        .controller('OvDistfwdmonCtrl',
        ['$log', '$scope', 'TableBuilderService',

            function ($log, $scope, tbs) {
            	tbs.buildTable({
            		scope:$scope,
            		tag:'distfwdmon'
            	});

                $log.log('OvDistfwdmonCtrl has been created');
            }]);
}());
