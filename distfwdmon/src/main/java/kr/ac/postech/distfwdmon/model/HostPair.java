/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kr.ac.postech.distfwdmon.model;

import org.onosproject.net.HostId;

public class HostPair {

    private HostId src;
    private HostId dst;

    public HostPair(HostId src, HostId dst) {
        this.src = src;
        this.dst = dst;
    }

    public HostId getSrc() {
        return src;
    }

    public HostId getDst() {
        return dst;
    }

    public void setSrc(HostId src) {
        this.src = src;
    }

    public void setDst(HostId dst) {
        this.dst = dst;
    }

    @Override
    public boolean equals(Object o) {
        HostPair hp = (HostPair) o;
        return hp.getSrc().mac().equals(src.mac())
                && hp.getDst().mac().equals(dst.mac());
    }

    @Override
    public int hashCode() {
        String ttl = src.mac().toString() + "_" + dst.mac().toString();
        return ttl.hashCode();
    }
}
