/*
 * Copyright 2014 Open Networking Laboratory
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package kr.ac.postech.distfwdmon.cli;

import java.util.concurrent.ConcurrentMap;

import kr.ac.postech.distfwdmon.DistFwdMonService;
import kr.ac.postech.distfwdmon.model.HostPair;

import org.apache.karaf.shell.commands.Argument;
import org.apache.karaf.shell.commands.Command;
import org.onosproject.cli.AbstractShellCommand;
import org.onosproject.net.DeviceId;
import org.onosproject.store.service.ConsistentMap;

/**
 * A demo service that lists the requested src to dst host pair and counter
 */
@Command(scope = "onos", name = "distfwdmon", description = "Lists the requested host pair the counter")
public class DistFwdMonCommand extends AbstractShellCommand {

    private static final String DEVICE_FMT = "========== %s ==========";
    private static final String HOST_FMT = "src=%s, dst=%s, counter=%d";

    @Argument(index = 0, name = "deviceId", description = "Device ID of switch", required = false, multiValued = false)
    private String deviceId = null;

    private DistFwdMonService service;
    private ConsistentMap<DeviceId, ConcurrentMap<HostPair, Long>> map;

    @Override
    protected void execute() {
        service = get(DistFwdMonService.class);
        map = service.getMap();

        DeviceId device;

        if (deviceId != null) {
            device = DeviceId.deviceId(deviceId);
            if (!map.containsKey(device)) {
                return;
            }

            map.get(device).value().forEach((k, v) -> print(HOST_FMT, k.getSrc(),
                                                    k.getDst(), v));
        } else {
            for (DeviceId devId : map.keySet()) {
                print(DEVICE_FMT, devId);

                map.get(devId).value().forEach((k, v) -> print(HOST_FMT, k.getSrc(),
                                                       k.getDst(), v));
            }
        }
    }
}
